/*
a. 213-46-8915 - Marjorie Green
BU1032 - The Busy Executive's Database Guide
BU2075 - You Can Combat Computer Stress!

b. 267-41-2394 - Muchecl O,Leary
BU1111 - Cooking with Computers 
TC7777 - NF

c. BU1032 - The Busy Executive's Database Guide
213-46-8915 - Green, Marjorie
409-56-7008 - Bennet, Abraham

d. PC1035 - But is it User Friendly 
1389 - Aldodata Infosystem

e. 1389 - Aldodata Infosystem
PC1035 - But is it User Friendly 
BU1032 - The Busy Executive's Database Guide
BU1111 - Cooking with Computers 
BU7832 - Straight Talk AboutComputer
PC8888 - Secrets of Silicon Valley
PC9999 - Net Etiqutte
*/
-- CREATING THE DATABASE
CREATE DATABASE BLOG_DB;

-- AUTHORS
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(50) NOT NULL,
password VARCHAR(50) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- POSTS
CREATE TABLE posts( 
id INT NOT NULL AUTO_INCREMENT, 
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL, 
content VARCHAR(5000) NOT NULL, 
datetime_posted DATETIME NOT NULL, 
PRIMARY KEY(id), 
CONSTRAINT fk_posts_users_id 
FOREIGN KEY(author_id) REFERENCES users(id) 
ON UPDATE CASCADE 
ON DELETE RESTRICT 
);

-- POST_COMMENTS
CREATE TABLE post_comments( 
id INT NOT NULL AUTO_INCREMENT, 
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000) NOT NULL, 
datetime_commented DATETIME NOT NULL, 
PRIMARY KEY(id), 
CONSTRAINT fk_post_comments_posts_id 
FOREIGN KEY(post_id) REFERENCES posts(id) 
ON UPDATE CASCADE 
ON DELETE RESTRICT, 
CONSTRAINT fk_post_comments_users_id 
FOREIGN KEY(user_id) REFERENCES users(id) 
ON UPDATE CASCADE 
ON DELETE RESTRICT
);

-- POST_LIKES
CREATE TABLE post_likes( 
id INT NOT NULL AUTO_INCREMENT, 
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL, 
PRIMARY KEY(id), 
CONSTRAINT fk_post_likes_posts_id 
FOREIGN KEY(post_id) REFERENCES posts(id) 
ON UPDATE CASCADE 
ON DELETE RESTRICT, 
CONSTRAINT fk_post_likes_users_id 
FOREIGN KEY(user_id) REFERENCES users(id) 
ON UPDATE CASCADE 
ON DELETE RESTRICT
);